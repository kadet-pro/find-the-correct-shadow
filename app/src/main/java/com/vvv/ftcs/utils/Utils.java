package com.vvv.ftcs.utils;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

/**
 * Created by Valera on 01.09.2017.
 */

public class Utils {
    public static void openAppInMarket(Activity activity) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + activity.getPackageName())));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + activity.getPackageName())));
        }
    }

    public static Intent getAccountIntent(String description) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return AccountManager.newChooseAccountIntent(null, null, new String[]{"com.google"},
                    description, null, null, null);
        } else {
            return AccountManager.newChooseAccountIntent(null, null, new String[]{"com.google"},
                    true, description, null, null, null);
        }
    }

    public static void openFacebook(Context context) {
        Intent i = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.facebook.com/vvvteamreal"));
        context.startActivity(i);
    }

    public static void openPrivacyPolicy(Context context) {
        Intent i = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://vvvteam.ru/privacy_policy.html"));
        context.startActivity(i);
    }

}
