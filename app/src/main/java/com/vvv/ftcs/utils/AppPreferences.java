package com.vvv.ftcs.utils;

import android.content.Context;

import com.orhanobut.hawk.Hawk;

/**
 * Created by Valera on 29.08.2017.
 */

public class AppPreferences {
    public AppPreferences(Context context) {
        Hawk.init(context).build();
    }

    public boolean isFirstInit() {
        return Hawk.get("firstInit", true);
    }

    public void setFistInit(boolean fistInit) {
        Hawk.put("firstInit", fistInit);
    }

    public boolean isMusicInclude() {
        return Hawk.get("musicInclude", true);
    }

    public void setMusicInclude(boolean musicInclude) {
        Hawk.put("musicInclude", musicInclude);
    }

    public void setSoundInclude(boolean soundInclude) {
        Hawk.put("soundInclude", soundInclude);
    }

    public boolean isSoundInclude() {
        return Hawk.get("soundInclude", true);
    }
}
