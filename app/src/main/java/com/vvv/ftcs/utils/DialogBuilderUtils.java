package com.vvv.ftcs.utils;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.vvv.ftcs.R;

import java.util.Collections;

import static com.google.android.gms.internal.zzahf.runOnUiThread;

/**
 * Created by Valera on 04.09.2017.
 */

public class DialogBuilderUtils {


    public static void show(Activity activity,  String message, OnDialogListener listener) {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_info);

        TextView vMessage = (TextView) dialog.findViewById(R.id.message);
        if (!TextUtils.isEmpty(message))
            vMessage.setText(message);
        View vOk = dialog.findViewById(R.id.ok);
        vOk.setOnClickListener(v -> {
            dialog.dismiss();

            new Thread(() -> {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(() -> {
                    if (listener != null)
                        listener.onClickOk();
                });
            }).start();
        });
        dialog.show();
    }


    public static void showLevelDone(Activity activity, String message, OnDialogListenerGame listener) {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_level_done);

        TextView vMessage = (TextView) dialog.findViewById(R.id.message);
        if (!TextUtils.isEmpty(message))
            vMessage.setText(message);

        (dialog.findViewById(R.id.close)).setOnClickListener(v -> {
            dialog.dismiss();

            new Thread(() -> {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(() -> {
                    if (listener != null)
                        listener.onClickClose();
                });
            }).start();
        });
        (dialog.findViewById(R.id.repeat)).setOnClickListener(v -> {
            dialog.dismiss();

            new Thread(() -> {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(() -> {
                    if (listener != null)
                        listener.onClickRepeat();
                });
            }).start();

        });
        (dialog.findViewById(R.id.next_level)).startAnimation(AnimationUtils.loadAnimation(activity, R.anim.alive_button));
        (dialog.findViewById(R.id.next_level)).setOnClickListener(v -> {
            dialog.dismiss();

            new Thread(() -> {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(() -> {
                    if (listener != null)
                        listener.onClickNextLevel();
                });
            }).start();
        });
        dialog.show();
    }

    public static void showLevelLost(Activity activity, String message, OnDialogListenerGame listener) {
        final Dialog dialog = new Dialog(activity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_level_lost);

        TextView vMessage = (TextView) dialog.findViewById(R.id.message);
        if (!TextUtils.isEmpty(message))
            vMessage.setText(message);
        (dialog.findViewById(R.id.close)).setOnClickListener(v -> {
            dialog.dismiss();

            new Thread(() -> {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(() -> {
                    if (listener != null)
                        listener.onClickClose();
                });
            }).start();

        });
        (dialog.findViewById(R.id.repeat)).setOnClickListener(v -> {
            dialog.dismiss();


            new Thread(() -> {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(() -> {
                    if (listener != null)
                        listener.onClickRepeat();
                });
            }).start();
        });
        dialog.show();
    }

    public interface OnDialogListener {
        void onClickOk();
    }

    public interface OnDialogListenerGame {
        void onClickRepeat();

        void onClickNextLevel();

        void onClickClose();
    }
}
