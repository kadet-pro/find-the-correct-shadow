package com.vvv.ftcs.utils;

import com.vvv.ftcs.R;
import com.vvv.ftcs.database.RealmDataBase;
import com.vvv.ftcs.models.LevelGame;
import com.vvv.ftcs.models.LevelGameDetails;
import com.vvv.ftcs.models.LevelInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Valera on 30.08.2017.
 */

public class LevelUtils {
    public static void create() {
        RealmDataBase realmDataBase = new RealmDataBase();
        for (int i = 0; i < GameCore.COUNT_LEVEL; i++) {
            realmDataBase.addLevel(new LevelInfo(i + 1, i == 0 ? true : false, 0));
        }
    }

    public static LevelGame getLevelGame(int level) {
        List<LevelGameDetails> list = new ArrayList<>();
        switch (level) {
            case 1:
                list.add(new LevelGameDetails(R.drawable.level1_1, false));
                list.add(new LevelGameDetails(R.drawable.level1_r, true));
                list.add(new LevelGameDetails(R.drawable.level1_4, false));
                list.add(new LevelGameDetails(R.drawable.level1_5, false));
                return new LevelGame(1, R.drawable.level1_color, list);
            case 2:
                list.add(new LevelGameDetails(R.drawable.level2_1, false));
                list.add(new LevelGameDetails(R.drawable.level2_r, true));
                list.add(new LevelGameDetails(R.drawable.level2_4, false));
                list.add(new LevelGameDetails(R.drawable.level2_5, false));
                return new LevelGame(2, R.drawable.level2_color, list);
            case 3:
                list.add(new LevelGameDetails(R.drawable.level3_1, false));
                list.add(new LevelGameDetails(R.drawable.level3_r, true));
                list.add(new LevelGameDetails(R.drawable.level3_2, false));
                list.add(new LevelGameDetails(R.drawable.level3_3, false));
                return new LevelGame(3, R.drawable.level3_color, list);
            case 4:
                list.add(new LevelGameDetails(R.drawable.level4_1, false));
                list.add(new LevelGameDetails(R.drawable.level4_r, true));
                list.add(new LevelGameDetails(R.drawable.level4_2, false));
                list.add(new LevelGameDetails(R.drawable.level4_3, false));
                return new LevelGame(4, R.drawable.level4_color, list);
            case 5:
                list.add(new LevelGameDetails(R.drawable.level5_1, false));
                list.add(new LevelGameDetails(R.drawable.level5_r, true));
                list.add(new LevelGameDetails(R.drawable.level5_2, false));
                list.add(new LevelGameDetails(R.drawable.level5_3, false));
                return new LevelGame(5, R.drawable.level5_color, list);
            case 6:
                list.add(new LevelGameDetails(R.drawable.level6_1, false));
                list.add(new LevelGameDetails(R.drawable.level6_r, true));
                list.add(new LevelGameDetails(R.drawable.level6_2, false));
                list.add(new LevelGameDetails(R.drawable.level6_3, false));
                return new LevelGame(6, R.drawable.level6_color, list);
            case 7:
                list.add(new LevelGameDetails(R.drawable.level7_1, false));
                list.add(new LevelGameDetails(R.drawable.level7_r, true));
                list.add(new LevelGameDetails(R.drawable.level7_2, false));
                list.add(new LevelGameDetails(R.drawable.level7_3, false));
                return new LevelGame(7, R.drawable.level7_color, list);
            case 8:
                list.add(new LevelGameDetails(R.drawable.level8_1, false));
                list.add(new LevelGameDetails(R.drawable.level8_r, true));
                list.add(new LevelGameDetails(R.drawable.level8_2, false));
                list.add(new LevelGameDetails(R.drawable.level8_3, false));
                return new LevelGame(8, R.drawable.level8_color, list);
            case 9:
                list.add(new LevelGameDetails(R.drawable.level9_1, false));
                list.add(new LevelGameDetails(R.drawable.level9_r, true));
                list.add(new LevelGameDetails(R.drawable.level9_2, false));
                list.add(new LevelGameDetails(R.drawable.level9_3, false));
                return new LevelGame(9, R.drawable.level9_color, list);
            case 10:
                list.add(new LevelGameDetails(R.drawable.level10_1, false));
                list.add(new LevelGameDetails(R.drawable.level10_r, true));
                list.add(new LevelGameDetails(R.drawable.level10_2, false));
                list.add(new LevelGameDetails(R.drawable.level10_3, false));
                return new LevelGame(10, R.drawable.level10_color, list);
            case 11:
                list.add(new LevelGameDetails(R.drawable.level11_1, false));
                list.add(new LevelGameDetails(R.drawable.level11_r, true));
                list.add(new LevelGameDetails(R.drawable.level11_2, false));
                list.add(new LevelGameDetails(R.drawable.level11_3, false));
                return new LevelGame(11, R.drawable.level11_color, list);
            case 12:
                list.add(new LevelGameDetails(R.drawable.level12_1, false));
                list.add(new LevelGameDetails(R.drawable.level12_r, true));
                list.add(new LevelGameDetails(R.drawable.level12_2, false));
                list.add(new LevelGameDetails(R.drawable.level12_3, false));
                return new LevelGame(12, R.drawable.level12_color, list);
            case 13:
                list.add(new LevelGameDetails(R.drawable.level13_1, false));
                list.add(new LevelGameDetails(R.drawable.level13_r, true));
                list.add(new LevelGameDetails(R.drawable.level13_2, false));
                list.add(new LevelGameDetails(R.drawable.level13_3, false));
                return new LevelGame(13, R.drawable.level13_color, list);
            case 14:
                list.add(new LevelGameDetails(R.drawable.level14_1, false));
                list.add(new LevelGameDetails(R.drawable.level14_r, true));
                list.add(new LevelGameDetails(R.drawable.level14_2, false));
                list.add(new LevelGameDetails(R.drawable.level14_3, false));
                return new LevelGame(14, R.drawable.level14_color, list);
            case 15:
                list.add(new LevelGameDetails(R.drawable.level15_1, false));
                list.add(new LevelGameDetails(R.drawable.level15_r, true));
                list.add(new LevelGameDetails(R.drawable.level15_2, false));
                list.add(new LevelGameDetails(R.drawable.level15_3, false));
                return new LevelGame(15, R.drawable.level15_color, list);
            case 16:
                list.add(new LevelGameDetails(R.drawable.level16_1, false));
                list.add(new LevelGameDetails(R.drawable.level16_r, true));
                list.add(new LevelGameDetails(R.drawable.level16_2, false));
                list.add(new LevelGameDetails(R.drawable.level16_3, false));
                return new LevelGame(16, R.drawable.level16_color, list);
            case 17:
                list.add(new LevelGameDetails(R.drawable.level17_1, false));
                list.add(new LevelGameDetails(R.drawable.level17_r, true));
                list.add(new LevelGameDetails(R.drawable.level17_2, false));
                list.add(new LevelGameDetails(R.drawable.level17_3, false));
                return new LevelGame(17, R.drawable.level17_color, list);
            case 18:
                list.add(new LevelGameDetails(R.drawable.level18_1, false));
                list.add(new LevelGameDetails(R.drawable.level18_r, true));
                list.add(new LevelGameDetails(R.drawable.level18_2, false));
                list.add(new LevelGameDetails(R.drawable.level18_3, false));
                return new LevelGame(18, R.drawable.level18_color, list);
            case 19:
                list.add(new LevelGameDetails(R.drawable.level19_1, false));
                list.add(new LevelGameDetails(R.drawable.level19_r, true));
                list.add(new LevelGameDetails(R.drawable.level19_2, false));
                list.add(new LevelGameDetails(R.drawable.level19_3, false));
                return new LevelGame(19, R.drawable.level19_color, list);
            case 20:
                list.add(new LevelGameDetails(R.drawable.level20_1, false));
                list.add(new LevelGameDetails(R.drawable.level20_r, true));
                list.add(new LevelGameDetails(R.drawable.level20_2, false));
                list.add(new LevelGameDetails(R.drawable.level20_3, false));
                return new LevelGame(20, R.drawable.level20_color, list);
            case 21:
                list.add(new LevelGameDetails(R.drawable.level21_1, false));
                list.add(new LevelGameDetails(R.drawable.level21_r, true));
                list.add(new LevelGameDetails(R.drawable.level21_2, false));
                list.add(new LevelGameDetails(R.drawable.level21_3, false));
                return new LevelGame(21, R.drawable.level21_color, list);
            case 22:
                list.add(new LevelGameDetails(R.drawable.level22_1, false));
                list.add(new LevelGameDetails(R.drawable.level22_r, true));
                list.add(new LevelGameDetails(R.drawable.level22_2, false));
                list.add(new LevelGameDetails(R.drawable.level22_3, false));
                return new LevelGame(22, R.drawable.level22_color, list);
            case 23:
                list.add(new LevelGameDetails(R.drawable.level23_1, false));
                list.add(new LevelGameDetails(R.drawable.level23_r, true));
                list.add(new LevelGameDetails(R.drawable.level23_2, false));
                list.add(new LevelGameDetails(R.drawable.level23_3, false));
                return new LevelGame(23, R.drawable.level23_color, list);
            case 24:
                list.add(new LevelGameDetails(R.drawable.level24_1, false));
                list.add(new LevelGameDetails(R.drawable.level24_r, true));
                list.add(new LevelGameDetails(R.drawable.level24_2, false));
                list.add(new LevelGameDetails(R.drawable.level24_3, false));
                return new LevelGame(24, R.drawable.level24_color, list);
            case 25:
                list.add(new LevelGameDetails(R.drawable.level25_1, false));
                list.add(new LevelGameDetails(R.drawable.level25_r, true));
                list.add(new LevelGameDetails(R.drawable.level25_2, false));
                list.add(new LevelGameDetails(R.drawable.level25_3, false));
                return new LevelGame(25, R.drawable.level25_color, list);
            case 26:
                list.add(new LevelGameDetails(R.drawable.level26_1, false));
                list.add(new LevelGameDetails(R.drawable.level26_r, true));
                list.add(new LevelGameDetails(R.drawable.level26_2, false));
                list.add(new LevelGameDetails(R.drawable.level26_3, false));
                return new LevelGame(26, R.drawable.level26_color, list);
            case 27:
                list.add(new LevelGameDetails(R.drawable.level27_1, false));
                list.add(new LevelGameDetails(R.drawable.level27_r, true));
                list.add(new LevelGameDetails(R.drawable.level27_2, false));
                list.add(new LevelGameDetails(R.drawable.level27_3, false));
                return new LevelGame(27, R.drawable.level27_color, list);
            case 28:
                list.add(new LevelGameDetails(R.drawable.level28_1, false));
                list.add(new LevelGameDetails(R.drawable.level28_r, true));
                list.add(new LevelGameDetails(R.drawable.level28_2, false));
                list.add(new LevelGameDetails(R.drawable.level28_3, false));
                return new LevelGame(28, R.drawable.level28_color, list);
            case 29:
                list.add(new LevelGameDetails(R.drawable.level29_1, false));
                list.add(new LevelGameDetails(R.drawable.level29_r, true));
                list.add(new LevelGameDetails(R.drawable.level29_2, false));
                list.add(new LevelGameDetails(R.drawable.level29_3, false));
                return new LevelGame(29, R.drawable.level29_color, list);
            case 30:
                list.add(new LevelGameDetails(R.drawable.level30_1, false));
                list.add(new LevelGameDetails(R.drawable.level30_r, true));
                list.add(new LevelGameDetails(R.drawable.level30_2, false));
                list.add(new LevelGameDetails(R.drawable.level30_3, false));
                return new LevelGame(30, R.drawable.level30_color, list);
            case 31:
                list.add(new LevelGameDetails(R.drawable.level31_1, false));
                list.add(new LevelGameDetails(R.drawable.level31_r, true));
                list.add(new LevelGameDetails(R.drawable.level31_2, false));
                list.add(new LevelGameDetails(R.drawable.level31_3, false));
                return new LevelGame(31, R.drawable.level31_color, list);
            case 32:
                list.add(new LevelGameDetails(R.drawable.level32_1, false));
                list.add(new LevelGameDetails(R.drawable.level32_r, true));
                list.add(new LevelGameDetails(R.drawable.level32_2, false));
                list.add(new LevelGameDetails(R.drawable.level32_3, false));
                return new LevelGame(32, R.drawable.level32_color, list);
            case 33:
                list.add(new LevelGameDetails(R.drawable.level33_1, false));
                list.add(new LevelGameDetails(R.drawable.level33_r, true));
                list.add(new LevelGameDetails(R.drawable.level33_2, false));
                list.add(new LevelGameDetails(R.drawable.level33_3, false));
                return new LevelGame(33, R.drawable.level33_color, list);
            case 34:
                list.add(new LevelGameDetails(R.drawable.level34_1, false));
                list.add(new LevelGameDetails(R.drawable.level34_r, true));
                list.add(new LevelGameDetails(R.drawable.level34_2, false));
                list.add(new LevelGameDetails(R.drawable.level34_3, false));
                return new LevelGame(34, R.drawable.level34_color, list);
            case 35:
                list.add(new LevelGameDetails(R.drawable.level35_1, false));
                list.add(new LevelGameDetails(R.drawable.level35_r, true));
                list.add(new LevelGameDetails(R.drawable.level35_2, false));
                list.add(new LevelGameDetails(R.drawable.level35_3, false));
                return new LevelGame(35, R.drawable.level35_color, list);
            case 36:
                list.add(new LevelGameDetails(R.drawable.level36_1, false));
                list.add(new LevelGameDetails(R.drawable.level36_r, true));
                list.add(new LevelGameDetails(R.drawable.level36_2, false));
                list.add(new LevelGameDetails(R.drawable.level36_3, false));
                return new LevelGame(36, R.drawable.level36_color, list);
            case 37:
                list.add(new LevelGameDetails(R.drawable.level37_1, false));
                list.add(new LevelGameDetails(R.drawable.level37_r, true));
                list.add(new LevelGameDetails(R.drawable.level37_2, false));
                list.add(new LevelGameDetails(R.drawable.level37_3, false));
                return new LevelGame(37, R.drawable.level37_color, list);
            case 38:
                list.add(new LevelGameDetails(R.drawable.level38_1, false));
                list.add(new LevelGameDetails(R.drawable.level38_r, true));
                list.add(new LevelGameDetails(R.drawable.level38_2, false));
                list.add(new LevelGameDetails(R.drawable.level38_3, false));
                return new LevelGame(38, R.drawable.level38_color, list);
            case 39:
                list.add(new LevelGameDetails(R.drawable.level39_1, false));
                list.add(new LevelGameDetails(R.drawable.level39_r, true));
                list.add(new LevelGameDetails(R.drawable.level39_2, false));
                list.add(new LevelGameDetails(R.drawable.level39_3, false));
                return new LevelGame(39, R.drawable.level39_color, list);
            case 40:
                list.add(new LevelGameDetails(R.drawable.level40_1, false));
                list.add(new LevelGameDetails(R.drawable.level40_r, true));
                list.add(new LevelGameDetails(R.drawable.level40_2, false));
                list.add(new LevelGameDetails(R.drawable.level40_3, false));
                return new LevelGame(40, R.drawable.level40_color, list);
            case 41:
                list.add(new LevelGameDetails(R.drawable.level41_1, false));
                list.add(new LevelGameDetails(R.drawable.level41_r, true));
                list.add(new LevelGameDetails(R.drawable.level41_2, false));
                list.add(new LevelGameDetails(R.drawable.level41_3, false));
                return new LevelGame(41, R.drawable.level41_color, list);
            case 42:
                list.add(new LevelGameDetails(R.drawable.level42_1, false));
                list.add(new LevelGameDetails(R.drawable.level42_r, true));
                list.add(new LevelGameDetails(R.drawable.level42_2, false));
                list.add(new LevelGameDetails(R.drawable.level42_3, false));
                return new LevelGame(42, R.drawable.level42_color, list);
            case 43:
                list.add(new LevelGameDetails(R.drawable.level43_1, false));
                list.add(new LevelGameDetails(R.drawable.level43_r, true));
                list.add(new LevelGameDetails(R.drawable.level43_2, false));
                list.add(new LevelGameDetails(R.drawable.level43_3, false));
                return new LevelGame(43, R.drawable.level43_color, list);
            case 44:
                list.add(new LevelGameDetails(R.drawable.level44_1, false));
                list.add(new LevelGameDetails(R.drawable.level44_r, true));
                list.add(new LevelGameDetails(R.drawable.level44_2, false));
                list.add(new LevelGameDetails(R.drawable.level44_3, false));
                return new LevelGame(44, R.drawable.level44_color, list);
            case 45:
                list.add(new LevelGameDetails(R.drawable.level45_1, false));
                list.add(new LevelGameDetails(R.drawable.level45_r, true));
                list.add(new LevelGameDetails(R.drawable.level45_2, false));
                list.add(new LevelGameDetails(R.drawable.level45_3, false));
                return new LevelGame(45, R.drawable.level45_color, list);
            case 46:
                list.add(new LevelGameDetails(R.drawable.level46_1, false));
                list.add(new LevelGameDetails(R.drawable.level46_r, true));
                list.add(new LevelGameDetails(R.drawable.level46_2, false));
                list.add(new LevelGameDetails(R.drawable.level46_3, false));
                return new LevelGame(46, R.drawable.level46_color, list);
            case 47:
                list.add(new LevelGameDetails(R.drawable.level47_1, false));
                list.add(new LevelGameDetails(R.drawable.level47_r, true));
                list.add(new LevelGameDetails(R.drawable.level47_2, false));
                list.add(new LevelGameDetails(R.drawable.level47_3, false));
                return new LevelGame(47, R.drawable.level47_color, list);
            case 48:
                list.add(new LevelGameDetails(R.drawable.level48_1, false));
                list.add(new LevelGameDetails(R.drawable.level48_r, true));
                list.add(new LevelGameDetails(R.drawable.level48_2, false));
                list.add(new LevelGameDetails(R.drawable.level48_3, false));
                return new LevelGame(48, R.drawable.level48_color, list);
            case 49:
                list.add(new LevelGameDetails(R.drawable.level49_1, false));
                list.add(new LevelGameDetails(R.drawable.level49_r, true));
                list.add(new LevelGameDetails(R.drawable.level49_2, false));
                list.add(new LevelGameDetails(R.drawable.level49_3, false));
                return new LevelGame(49, R.drawable.level49_color, list);
            case 50:
                list.add(new LevelGameDetails(R.drawable.level50_1, false));
                list.add(new LevelGameDetails(R.drawable.level50_r, true));
                list.add(new LevelGameDetails(R.drawable.level50_2, false));
                list.add(new LevelGameDetails(R.drawable.level50_3, false));
                return new LevelGame(50, R.drawable.level50_color, list);
            case 51:
                list.add(new LevelGameDetails(R.drawable.level51_1, false));
                list.add(new LevelGameDetails(R.drawable.level51_r, true));
                list.add(new LevelGameDetails(R.drawable.level51_2, false));
                list.add(new LevelGameDetails(R.drawable.level51_3, false));
                return new LevelGame(51, R.drawable.level51_color, list);
            case 52:
                list.add(new LevelGameDetails(R.drawable.level52_1, false));
                list.add(new LevelGameDetails(R.drawable.level52_r, true));
                list.add(new LevelGameDetails(R.drawable.level52_2, false));
                list.add(new LevelGameDetails(R.drawable.level52_3, false));
                return new LevelGame(52, R.drawable.level52_color, list);
            case 53:
                list.add(new LevelGameDetails(R.drawable.level53_1, false));
                list.add(new LevelGameDetails(R.drawable.level53_r, true));
                list.add(new LevelGameDetails(R.drawable.level53_2, false));
                list.add(new LevelGameDetails(R.drawable.level53_3, false));
                return new LevelGame(53, R.drawable.level53_color, list);
            case 54:
                list.add(new LevelGameDetails(R.drawable.level54_1, false));
                list.add(new LevelGameDetails(R.drawable.level54_r, true));
                list.add(new LevelGameDetails(R.drawable.level54_2, false));
                list.add(new LevelGameDetails(R.drawable.level54_3, false));
                return new LevelGame(54, R.drawable.level54_color, list);
            case 55:
                list.add(new LevelGameDetails(R.drawable.level55_1, false));
                list.add(new LevelGameDetails(R.drawable.level55_r, true));
                list.add(new LevelGameDetails(R.drawable.level55_2, false));
                list.add(new LevelGameDetails(R.drawable.level55_3, false));
                return new LevelGame(55, R.drawable.level55_color, list);
            case 56:
                list.add(new LevelGameDetails(R.drawable.level56_1, false));
                list.add(new LevelGameDetails(R.drawable.level56_r, true));
                list.add(new LevelGameDetails(R.drawable.level56_2, false));
                list.add(new LevelGameDetails(R.drawable.level56_3, false));
                return new LevelGame(56, R.drawable.level56_color, list);
            case 57:
                list.add(new LevelGameDetails(R.drawable.level57_1, false));
                list.add(new LevelGameDetails(R.drawable.level57_r, true));
                list.add(new LevelGameDetails(R.drawable.level57_2, false));
                list.add(new LevelGameDetails(R.drawable.level57_3, false));
                return new LevelGame(57, R.drawable.level57_color, list);
            case 58:
                list.add(new LevelGameDetails(R.drawable.level58_1, false));
                list.add(new LevelGameDetails(R.drawable.level58_r, true));
                list.add(new LevelGameDetails(R.drawable.level58_2, false));
                list.add(new LevelGameDetails(R.drawable.level58_3, false));
                return new LevelGame(58, R.drawable.level58_color, list);
            case 59:
                list.add(new LevelGameDetails(R.drawable.level59_1, false));
                list.add(new LevelGameDetails(R.drawable.level59_r, true));
                list.add(new LevelGameDetails(R.drawable.level59_2, false));
                list.add(new LevelGameDetails(R.drawable.level59_3, false));
                return new LevelGame(59, R.drawable.level59_color, list);
            case 60:
                list.add(new LevelGameDetails(R.drawable.level60_1, false));
                list.add(new LevelGameDetails(R.drawable.level60_r, true));
                list.add(new LevelGameDetails(R.drawable.level60_2, false));
                list.add(new LevelGameDetails(R.drawable.level60_3, false));
                return new LevelGame(60, R.drawable.level60_color, list);
            default:
                return new LevelGame(); //last level
        }
    }
}
