package com.vvv.ftcs.database;

import com.vvv.ftcs.models.LevelInfo;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;

/**
 * Created by Valera on 30.08.2017.
 */

public class RealmDataBase {
    public Realm getRealm() {
        return Realm.getDefaultInstance();
    }

    public void addLevel(LevelInfo level) {
        Realm realm = getRealm();
        if (realm != null) {
            realm.beginTransaction();
            realm.insertOrUpdate(level);
            realm.commitTransaction();
            realm.close();
        }
    }

    public Observable<List<LevelInfo>> getLevels() {
        List<LevelInfo> list = new ArrayList<>();
        Realm realm = getRealm();
        if (realm != null) {
            list = realm.copyFromRealm(realm.where(LevelInfo.class).findAll());
            realm.close();
        }
        return Observable.just(list);
    }

    public int getNextLevel() {
        int idNext = -1;
        Realm realm = getRealm();
        if (realm != null) {
            idNext = realm.where(LevelInfo.class).equalTo("access", true).max("id").intValue();
            realm.close();
        }
        return idNext;
    }

    public void levelUP(int id) {
        Realm realm = getRealm();
        if (realm != null) {
            LevelInfo levelInfo = realm.where(LevelInfo.class).equalTo("id", id + 1).findFirst();
            if (levelInfo != null) {
                realm.beginTransaction();
                levelInfo.setAccess(true);
                realm.commitTransaction();
            }
            realm.close();
        }
    }
}