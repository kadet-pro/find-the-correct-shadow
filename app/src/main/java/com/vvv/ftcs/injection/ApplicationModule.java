package com.vvv.ftcs.injection;

import android.app.Application;
import android.content.Context;

import com.vvv.ftcs.database.RealmDataBase;
import com.vvv.ftcs.utils.AppPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Valera on 29.08.2017.
 */
@Module
public class ApplicationModule {
    private final Application mApplication;


    public ApplicationModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    public AppPreferences provideUser(Context context) {
        return new AppPreferences(context);
    }

    @Provides
    @Singleton
    public RealmDataBase provideRealmDataBase() {
        return new RealmDataBase();
    }

}
