package com.vvv.ftcs.injection;

import com.vvv.ftcs.ui.activitys.MainActivity;
import com.vvv.ftcs.ui.activitys.SplashActivity;
import com.vvv.ftcs.ui.fragments.GameFragment;
import com.vvv.ftcs.ui.fragments.LevelFragment;
import com.vvv.ftcs.ui.fragments.MenuFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Valera on 29.08.2017.
 */
@Component(modules = ApplicationModule.class)
@Singleton
public interface AppComponent {

    void inject(MainActivity activity);

    void inject(SplashActivity activity);

    void inject(MenuFragment fragment);

    void inject(LevelFragment fragment);

    void inject(GameFragment fragment);

}
