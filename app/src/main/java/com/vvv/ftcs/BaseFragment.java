package com.vvv.ftcs;

import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.vvv.ftcs.analytics.GoogleAnalyticsUtils;
import com.vvv.ftcs.utils.AppPreferences;

import io.reactivex.disposables.Disposable;

/**
 * Created by valera on 29.08.2017.
 */

public abstract class BaseFragment extends Fragment {

    protected Animation alive;

    private SoundPool sp;
    private int soundId;


    protected Disposable subscriber;

    protected abstract String getNameFragment();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        alive = AnimationUtils.loadAnimation(getContext(), R.anim.alive_button);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            sp = new SoundPool.Builder().setMaxStreams(10).build();
        else
            sp = new SoundPool(10, AudioManager.STREAM_MUSIC, 1);

        sp.setOnLoadCompleteListener((soundPool, sampleId, status) -> {
        });
        soundId = sp.load(getContext(), R.raw.click, 1);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GoogleAnalyticsUtils.onShowScreen(getNameFragment());
        startAnimation();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (subscriber != null && subscriber.isDisposed()) {
            subscriber.dispose();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    protected abstract void startAnimation();

    protected void effectSoundClick(AppPreferences appPreferences) {
        if (sp != null && appPreferences.isMusicInclude())
            sp.play(soundId, 1, 1, 0, 0, 1);
    }
}
