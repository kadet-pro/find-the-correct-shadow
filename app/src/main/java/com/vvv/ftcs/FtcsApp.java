package com.vvv.ftcs;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.vvv.ftcs.analytics.GoogleAnalyticsUtils;
import com.vvv.ftcs.database.RealmDataBaseMigration;
import com.vvv.ftcs.injection.AppComponent;
import com.vvv.ftcs.injection.ApplicationModule;
import com.vvv.ftcs.injection.DaggerAppComponent;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Valera on 29.08.2017.
 */

public class FtcsApp extends Application {
    public static Context applicationContext;
    public static AppComponent component;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        applicationContext = getApplicationContext();
        component = DaggerAppComponent.builder().applicationModule(new ApplicationModule(this)).build();
        //realm
        Realm.init(this);
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder()
                .name("com.vvv.ftcs")
                .schemaVersion(1)
                .migration(new RealmDataBaseMigration());
        if (BuildConfig.DEBUG) {
            builder.deleteRealmIfMigrationNeeded();
        }
        Realm.setDefaultConfiguration(builder.build());
        GoogleAnalyticsUtils.init(this);
        //ads
    }

    public static Context getContext() {
        return applicationContext;
    }
}
