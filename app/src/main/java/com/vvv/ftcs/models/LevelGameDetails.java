package com.vvv.ftcs.models;

/**
 * Created by Valera on 31.08.2017.
 */

public class LevelGameDetails {
    private int image;
    private boolean right;

    public LevelGameDetails() {
    }

    public LevelGameDetails(int image, boolean right) {
        this.image = image;
        this.right = right;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }
}
