package com.vvv.ftcs.models;

import java.util.List;

/**
 * Created by Valera on 31.08.2017.
 */

public class LevelGame {

    private int id;
    private int sound = -1;
    private int image;
    private List<LevelGameDetails> items;

    public LevelGame() {
    }

    public LevelGame(int image, List<LevelGameDetails> items) {
        this.image = image;
        this.items = items;
    }


    public LevelGame(int id, int image, List<LevelGameDetails> items) {
        this.id = id;
        this.image = image;
        this.items = items;
    }

    public LevelGame(int id, int sound, int image, List<LevelGameDetails> items) {
        this.id = id;
        this.sound = sound;
        this.image = image;
        this.items = items;
    }

    public int getSound() {
        return sound;
    }

    public void setSound(int sound) {
        this.sound = sound;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public List<LevelGameDetails> getItems() {
        return items;
    }

    public void setItems(List<LevelGameDetails> items) {
        this.items = items;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
