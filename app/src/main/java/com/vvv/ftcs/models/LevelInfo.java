package com.vvv.ftcs.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Valera on 29.08.2017.
 */

public class LevelInfo extends RealmObject {
    @PrimaryKey
    private int id;

    private boolean access;
    private int star;

    public LevelInfo() {
    }

    public LevelInfo(int id, boolean access, int star) {
        this.id = id;
        this.access = access;
        this.star = star;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAccess() {
        return access;
    }

    public void setAccess(boolean access) {
        this.access = access;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }
}
