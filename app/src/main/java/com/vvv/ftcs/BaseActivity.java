package com.vvv.ftcs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.vvv.ftcs.analytics.GoogleAnalyticsUtils;

import io.reactivex.disposables.Disposable;

/**
 * Created by Valera on 29.08.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    protected Disposable subscriber;

    protected abstract String getNameScreen();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        GoogleAnalyticsUtils.onShowScreen(getNameScreen());
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (subscriber != null && subscriber.isDisposed()) {
            subscriber.dispose();
        }
    }
}
