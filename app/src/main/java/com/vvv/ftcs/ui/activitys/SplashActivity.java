package com.vvv.ftcs.ui.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.vvv.ftcs.FtcsApp;
import com.vvv.ftcs.R;
import com.vvv.ftcs.utils.AppPreferences;
import com.vvv.ftcs.utils.LevelUtils;

import javax.inject.Inject;

/**
 * Created by Valera on 31.08.2017.
 */

public class SplashActivity extends Activity {

    @Inject
    AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        FtcsApp.component.inject(this);
        init();
    }

    private void init() {
        if (appPreferences.isFirstInit()) {
            LevelUtils.create();
            appPreferences.setFistInit(false);
        }
        goToApp();
    }


    private void goToApp() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();
    }

}
