package com.vvv.ftcs.ui.fragments;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

import com.vvv.ftcs.BaseFragment;
import com.vvv.ftcs.FtcsApp;
import com.vvv.ftcs.R;
import com.vvv.ftcs.database.RealmDataBase;
import com.vvv.ftcs.databinding.FragmentGameBinding;
import com.vvv.ftcs.models.LevelGame;
import com.vvv.ftcs.ui.activitys.MainActivity;
import com.vvv.ftcs.utils.AppPreferences;
import com.vvv.ftcs.utils.DialogBuilderUtils;
import com.vvv.ftcs.utils.GameCore;
import com.vvv.ftcs.utils.LevelUtils;
import com.vvv.ftcs.utils.Utils;

import java.util.Collections;

import javax.inject.Inject;

import static com.google.android.gms.internal.zzahf.runOnUiThread;

/**
 * Created by Valera on 31.08.2017.
 */

public class GameFragment extends BaseFragment {
    private LevelGame levelGame;
    private boolean gameStart = false;
    private int idLevel = -1; //from level fragment
    private SoundPool sp;
    private boolean img1, img2, img3, img4;

    FragmentGameBinding mBinding;

    @Inject
    RealmDataBase realmDataBase;

    @Inject
    AppPreferences appPreferences;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            if (getArguments().containsKey("idLevel")) {
                idLevel = getArguments().getInt("idLevel");
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            sp = new SoundPool.Builder().setMaxStreams(10).build();
        else
            sp = new SoundPool(10, AudioManager.STREAM_MUSIC, 1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = FragmentGameBinding.inflate(inflater, container, false);
        FtcsApp.component.inject(this);
        return mBinding.getRoot();
    }

    @Override
    protected String getNameFragment() {
        return "Game Fragment";
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        levelGame = LevelUtils.getLevelGame(idLevel != -1 ? idLevel : realmDataBase.getNextLevel());
        mBinding.mainImage.setOnClickListener(v -> effectSoundLevel());
        mBinding.back.setOnClickListener(v -> closeFragment());
        mBinding.image1.setOnClickListener(v -> check(img1));
        mBinding.image2.setOnClickListener(v -> check(img2));
        mBinding.image3.setOnClickListener(v -> check(img3));
        mBinding.image4.setOnClickListener(v -> check(img4));
        startGame();
    }

    @Override
    protected void startAnimation() {

    }

    private void startGame() {
        if (levelGame != null) {
            animationObjects();
            mBinding.mainImage.setImageResource(levelGame.getImage());
            new Thread(() -> {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(() -> {
                    if (levelGame.getItems() != null && levelGame.getItems().size() > 0) {
                        Collections.shuffle(levelGame.getItems());
                        for (int i = 0; i < levelGame.getItems().size(); i++) {
                            if (i == 0) {
                                mBinding.image1.setImageResource(levelGame.getItems().get(i).getImage());
                                img1 = levelGame.getItems().get(i).isRight();
                            } else if (i == 1) {
                                mBinding.image2.setImageResource(levelGame.getItems().get(i).getImage());
                                img2 = levelGame.getItems().get(i).isRight();
                            } else if (i == 2) {
                                mBinding.image3.setImageResource(levelGame.getItems().get(i).getImage());
                                img3 = levelGame.getItems().get(i).isRight();
                            } else if (i == 3) {
                                mBinding.image4.setImageResource(levelGame.getItems().get(i).getImage());
                                img4 = levelGame.getItems().get(i).isRight();
                                break;
                            }
                        }
                    }
                    effectSoundLevel();
                });
            }).start();
        } else
            getActivity().onBackPressed();
    }

    private void stopGame() {
        gameStart = false;
         DialogBuilderUtils.showLevelLost(getActivity(),  getString(R.string.you_lose), new DialogBuilderUtils.OnDialogListenerGame() {
            @Override
            public void onClickRepeat() {
                startGame();
            }

            @Override
            public void onClickNextLevel() {

            }

            @Override
            public void onClickClose() {
                closeFragment();
            }
        });
    }

    private void levelDone() {
        realmDataBase.levelUP(levelGame.getId());
        gameStart = false;
        DialogBuilderUtils.showLevelDone(getActivity(),   getString(R.string.you_have_passed_the_level), new DialogBuilderUtils.OnDialogListenerGame() {
            @Override
            public void onClickRepeat() {
                startGame();
            }

            @Override
            public void onClickNextLevel() {
                nextLevel();
            }

            @Override
            public void onClickClose() {
                closeFragment();
            }
        });

    }

    private void nextLevel() {
        if (levelGame.getId() == GameCore.COUNT_LEVEL) {
            DialogBuilderUtils.show(getActivity(),   getString(R.string.you_passed_the_game), () -> {
                Utils.openAppInMarket(getActivity());
                closeFragment();
            });
        } else {
            levelGame = LevelUtils.getLevelGame(levelGame.getId() >= GameCore.COUNT_LEVEL ? levelGame.getId() : levelGame.getId() + 1);
            startGame();
        }
    }

    private void effectSoundLevel() {
        if (levelGame.getSound() != -1 && appPreferences.isMusicInclude()) {
            int sound = sp.load(getContext(), levelGame.getSound(), 1);
            sp.play(sound, 1, 1, 0, 0, 1);
        }
    }

    private void animationObjects() {
        RotateAnimation rotate = new RotateAnimation(0, 2160,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);

        rotate.setDuration(3000);
        rotate.setRepeatCount(0);
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                gameStart = true;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mBinding.imageVariant.startAnimation(rotate);
    }

    private void closeFragment() {
        effectSoundClick(appPreferences);
        getActivity().onBackPressed();
    }

    private void check(boolean state) {
        if (gameStart) {
            if (state) {
                levelDone();
            } else
                stopGame();
            effectSoundClick(appPreferences);
        }
    }
}
