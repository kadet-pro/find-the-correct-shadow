package com.vvv.ftcs.ui.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.vvv.ftcs.R;
import com.vvv.ftcs.models.LevelInfo;
import com.vvv.ftcs.ui.views.LevelItemView;

import java.util.List;

/**
 * Created by Valera on 06.09.2017.
 */

public class LevelPageAdapter extends PagerAdapter {
    private List<List<LevelInfo>> list;
    private LayoutInflater layoutInflater;
    private Context context;

    public LevelPageAdapter(Context context, List<List<LevelInfo>> list) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = this.layoutInflater.inflate(R.layout.view_container_levels, container, false);
        LinearLayout container1 = (LinearLayout) view.findViewById(R.id.container_1);
        LinearLayout container2 = (LinearLayout) view.findViewById(R.id.container_2);
        LinearLayout container3 = (LinearLayout) view.findViewById(R.id.container_3);
        List<LevelInfo> lists = list.get(position);
        Log.d("TAB_LOG", "instantiateItem size: " + lists.size());
        for (int i = 0; i < lists.size(); i++) {
            if ((i >= 0) && (i <= 3)) {
                LevelItemView levelItemView = new LevelItemView(context);
                levelItemView.setLevelInfo(lists.get(i));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewPager.LayoutParams.WRAP_CONTENT, ViewPager.LayoutParams.MATCH_PARENT);
                params.weight = 1.0f;
                levelItemView.setLayoutParams(params);
                container1.addView(levelItemView);
            } else if ((i >= 4) && (i <= 7)) {
                LevelItemView levelItemView = new LevelItemView(context);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewPager.LayoutParams.WRAP_CONTENT, ViewPager.LayoutParams.MATCH_PARENT);
                params.weight = 1.0f;
                levelItemView.setLayoutParams(params);
                levelItemView.setLevelInfo(lists.get(i));
                container2.addView(levelItemView);
            } else if ((i >= 8) && (i <= 11)) {
                LevelItemView levelItemView = new LevelItemView(context);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewPager.LayoutParams.WRAP_CONTENT, ViewPager.LayoutParams.MATCH_PARENT);
                params.weight = 1.0f;
                levelItemView.setLayoutParams(params);
                levelItemView.setLevelInfo(lists.get(i));
                container3.addView(levelItemView);
            }
        }
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
