package com.vvv.ftcs.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vvv.ftcs.BaseFragment;
import com.vvv.ftcs.FtcsApp;
import com.vvv.ftcs.R;
import com.vvv.ftcs.analytics.GoogleAnalyticsUtils;
import com.vvv.ftcs.databinding.FragmentMenuBinding;
import com.vvv.ftcs.ui.activitys.MainActivity;
import com.vvv.ftcs.utils.AppPreferences;
import com.vvv.ftcs.utils.Utils;

import javax.inject.Inject;


/**
 * Created by Valera on 29.08.2017.
 */

public class MenuFragment extends BaseFragment {
    FragmentMenuBinding mBinding;

    @Inject
    AppPreferences appPreferences;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = FragmentMenuBinding.inflate(inflater, container, false);
        FtcsApp.component.inject(this);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (appPreferences.isMusicInclude())
            mBinding.music.setImageResource(R.drawable.btn_music_on);
        else mBinding.music.setImageResource(R.drawable.btn_music_off);

        if (appPreferences.isSoundInclude())
            mBinding.sound.setImageResource(R.drawable.btn_sound_on);
        else mBinding.sound.setImageResource(R.drawable.btn_sound_off);

        mBinding.app.setOnClickListener(v -> {
            effectSoundClick(appPreferences);
            Utils.openAppInMarket(getActivity());
            GoogleAnalyticsUtils.onClickButton("app");
        });
        mBinding.play.setOnClickListener(v -> {
            effectSoundClick(appPreferences);
            ((MainActivity) getActivity()).playGame(-1);
            GoogleAnalyticsUtils.onClickButton("play");
        });
        mBinding.level.setOnClickListener(v -> {
            effectSoundClick(appPreferences);
            ((MainActivity) getActivity()).showLevel();
            GoogleAnalyticsUtils.onClickButton("level");
        });

        mBinding.facebook.setOnClickListener(view12 -> {
            Utils.openFacebook(getActivity());
            effectSoundClick(appPreferences);
            GoogleAnalyticsUtils.onClickButton("facebook");
        });

        mBinding.privacyPolicy.setOnClickListener(view12 -> {
            Utils.openPrivacyPolicy(getActivity());
            effectSoundClick(appPreferences);
            GoogleAnalyticsUtils.onClickButton("Privacy Policy");
        });

        mBinding.sound.setOnClickListener(view1 -> {
            if (appPreferences.isSoundInclude()) {
                appPreferences.setSoundInclude(false);
                mBinding.sound.setImageResource(R.drawable.btn_sound_off);
                ((MainActivity) getActivity()).musicMenu(false);

            } else {
                ((MainActivity) getActivity()).musicMenu(true);
                appPreferences.setSoundInclude(true);
                mBinding.sound.setImageResource(R.drawable.btn_sound_on);
            }
            effectSoundClick(appPreferences);
            GoogleAnalyticsUtils.onClickButton("sound " + appPreferences.isSoundInclude());
        });

        mBinding.music.setOnClickListener(v -> {
            if (appPreferences.isMusicInclude()) {
                appPreferences.setMusicInclude(false);
                mBinding.music.setImageResource(R.drawable.btn_music_off);
            } else {
                appPreferences.setMusicInclude(true);
                mBinding.music.setImageResource(R.drawable.btn_music_on);
            }
            effectSoundClick(appPreferences);
            GoogleAnalyticsUtils.onClickButton("music " + appPreferences.isMusicInclude());
        });
    }

    @Override
    protected String getNameFragment() {
        return "Menu Fragment";
    }

    @Override
    protected void startAnimation() {
        mBinding.play.startAnimation(alive);
    }

}
