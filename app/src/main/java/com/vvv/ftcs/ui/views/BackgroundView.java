package com.vvv.ftcs.ui.views;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import com.vvv.ftcs.R;


import static com.google.android.gms.internal.zzahf.runOnUiThread;

/**
 * Created by valera on 17.09.2017.
 */

public class BackgroundView extends FrameLayout {
    private View cloudBig, cloudSmall;
    private View an1, an2, an3;
    private Thread anumAnimals;

    public BackgroundView(@NonNull Context context) {
        super(context);
        init();
    }

    public BackgroundView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BackgroundView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        addView(inflate(getContext(), R.layout.view_background, null));
        cloudBig = findViewById(R.id.cloud_big);
        cloudSmall = findViewById(R.id.cloud_small);
        an1 = findViewById(R.id.an1);
        an2 = findViewById(R.id.an2);
        an3 = findViewById(R.id.an3);
        tablet();
        startAnimClouds();
        startAnimAnimals();
    }

    private void startAnimClouds() {
        Animation cloudFast = AnimationUtils.loadAnimation(getContext(), R.anim.cloud_fast);
        Animation cloudSlow = AnimationUtils.loadAnimation(getContext(), R.anim.cloud_slow);
        cloudBig.startAnimation(cloudFast);
        cloudSmall.startAnimation(cloudSlow);
    }

    private int i = 0;

    private void startAnimAnimals() {
        anumAnimals = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    runOnUiThread(() -> {
                        if (i == 0) {
                            animAnimals(an1);
                            i = 1;
                        } else if (i == 1) {
                            animAnimals(an2);
                            i = 2;
                        } else {
                            animAnimals(an3);
                            i = 0;
                        }
                    });
                } catch (Exception ex) {
                    ex.printStackTrace();
                    break;
                }
            }
        });
        anumAnimals.start();
    }


    private void animAnimals(View an) {
        Animation anim1 = AnimationUtils.loadAnimation(getContext(), R.anim.slid_up);
        Animation anim2 = AnimationUtils.loadAnimation(getContext(), R.anim.slid_down);
        anim1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                an.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Thread(() -> {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(() -> an.startAnimation(anim2));
                }).start();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        anim2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                an.setVisibility(INVISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        an.startAnimation(anim1);
    }

    private void tablet() {
        if (((TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE)).getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
            LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            an1.setLayoutParams(lp);
            an2.setLayoutParams(lp);
            an3.setLayoutParams(lp);
        }
    }

    public void onDestroy() {
        if (anumAnimals != null)
            anumAnimals.interrupt();
    }
}
