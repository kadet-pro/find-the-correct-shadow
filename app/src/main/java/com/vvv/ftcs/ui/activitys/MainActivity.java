package com.vvv.ftcs.ui.activitys;

import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.vvv.ftcs.BaseActivity;
import com.vvv.ftcs.FtcsApp;
import com.vvv.ftcs.R;
import com.vvv.ftcs.databinding.ActivityMainBinding;
import com.vvv.ftcs.ui.fragments.GameFragment;
import com.vvv.ftcs.ui.fragments.LevelFragment;
import com.vvv.ftcs.ui.fragments.MenuFragment;
import com.vvv.ftcs.utils.AppPreferences;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {
    ActivityMainBinding mBinding;
    private MediaPlayer mp;

    @Inject
    AppPreferences appPreferences;

    @Override
    protected String getNameScreen() {
        return "Main Activity";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        FtcsApp.component.inject(this);
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-4526926388424501~6229862782");
        mBinding.adView.loadAd(new AdRequest.Builder().build());
        showMenu();
    }
 
    @Override
    protected void onStart() {
        super.onStart();
        musicMenu(appPreferences.isSoundInclude());
    }

    @Override
    protected void onPause() {
        if (mp != null && mp.isPlaying()) {
            mp.release();
            mp = null;
        }
        super.onPause();
    }


    public void showMenu() {
        setFragment(new MenuFragment(), false);
    }

    public void showLevel() {
        setFragment(new LevelFragment(), true);
    }

    public void playGame(int idLevel) {
        GameFragment gameFragment = new GameFragment();
        if (idLevel != -1) {
            Bundle bundle = new Bundle();
            bundle.putInt("idLevel", idLevel);
            gameFragment.setArguments(bundle);
        }
        setFragment(gameFragment, true);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    public void setFragment(Fragment fragment, boolean history) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, fragment);
        if (history)
            transaction.addToBackStack(fragment.getClass().getName());
        else
            getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        transaction.commit();
    }

    public void musicMenu(boolean state) {
        if (state) {
            mp = MediaPlayer.create(this, R.raw.back);
            mp.setLooping(true);
            mp.start();
        } else {
            if (mp != null)
                mp.pause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBinding.background.onDestroy();
    }
}
