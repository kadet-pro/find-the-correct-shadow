package com.vvv.ftcs.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eftimoff.viewpagertransformers.AccordionTransformer;
import com.vvv.ftcs.BaseFragment;
import com.vvv.ftcs.FtcsApp;
import com.vvv.ftcs.database.RealmDataBase;
import com.vvv.ftcs.databinding.FragmentLevelBinding;
import com.vvv.ftcs.models.LevelInfo;
import com.vvv.ftcs.ui.activitys.MainActivity;
import com.vvv.ftcs.ui.adapters.LevelPageAdapter;
import com.vvv.ftcs.utils.AppPreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Valera on 31.08.2017.
 */

public class LevelFragment extends BaseFragment {

    FragmentLevelBinding mBinding;

    LevelPageAdapter adapter;

    @Inject
    RealmDataBase realmDataBase;

    @Inject
    AppPreferences appPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = FragmentLevelBinding.inflate(inflater, container, false);
        FtcsApp.component.inject(this);
        return mBinding.getRoot();
    }

    @Override
    protected String getNameFragment() {
        return "Level Fragment";
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    @Override
    protected void startAnimation() {

    }

    private void initUI() {
        mBinding.back.setOnClickListener(v -> {
            effectSoundClick(appPreferences);
            getActivity().onBackPressed();
        });
        subscriber = realmDataBase.getLevels()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(levelInfos -> {
                    List<List<LevelInfo>> levelLists = new ArrayList<>();
                    for (int i = 0; i < 5; i++) {
                        List<LevelInfo> levels = new ArrayList<>();
                        int from = i == 0 ? 0 : i * 12;
                        int before = from + 12;
                        for (int ii = from; ii < before; ii++) {
                            levels.add(levelInfos.get(ii));
                        }
                        levelLists.add(levels);
                    }
                    adapter = new LevelPageAdapter(getContext(), levelLists);
                    mBinding.viewpager.setAdapter(adapter);
                    mBinding.indicator.setViewPager(mBinding.viewpager);
                    mBinding.viewpager.setPageTransformer(true, new AccordionTransformer());
                }, Throwable::printStackTrace);
    }

    @Subscribe
    public void onEvent(LevelInfo item) {
        if (item.isAccess()) {
            ((MainActivity) getActivity()).playGame(item.getId());
        }
        effectSoundClick(appPreferences);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
