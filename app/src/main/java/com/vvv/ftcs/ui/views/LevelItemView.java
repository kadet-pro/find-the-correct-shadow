package com.vvv.ftcs.ui.views;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.vvv.ftcs.R;
import com.vvv.ftcs.models.LevelInfo;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Valera on 06.09.2017.
 */

public class LevelItemView extends FrameLayout {
    public LevelInfo levelInfo;
    public View main;
    public View lock;
    public TextView numLevel;

    public LevelItemView(@NonNull Context context) {
        super(context);
        init();
    }

    public LevelItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LevelItemView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        addView(inflate(getContext(), R.layout.item_level, null));
        numLevel = (TextView) findViewById(R.id.num_level);
        main = findViewById(R.id.main);
        lock = findViewById(R.id.lock);
    }

    public void setLevelInfo(LevelInfo levelInfo) {
        this.levelInfo = levelInfo;
        numLevel.setText(String.valueOf(levelInfo.getId()));
        Log.d("tab_log", levelInfo.getId() + " setLevelInfo: " + levelInfo.isAccess());
        if (levelInfo.isAccess()) {
            numLevel.setVisibility(View.VISIBLE);
            lock.setVisibility(View.GONE);
        } else {
            numLevel.setVisibility(View.GONE);
            lock.setVisibility(View.VISIBLE);
        }

        main.setOnClickListener(v -> {
            EventBus.getDefault().post(this.levelInfo);
        });
    }
}
